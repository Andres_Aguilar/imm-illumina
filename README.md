** imm-illumina **
===================

Description
------------------

This mini pipeline prepares data from Illumina(MiSeq) to be processed by [ImmunediveRsity](https://bitbucket.org/ImmunediveRsity/immunediversity)

It uses [PANDAseq](https://github.com/neufeld/pandaseq) for assamble pairend sequences,

[seqtk](https://github.com/lh3/seqtk) to get the reverse complement and

[R-project](http://cran.r-project.org/) with [BioConductor/ShortRead](http://www.bioconductor.org/packages/2.14/bioc/html/ShortRead.html) to make a subset.


Requirements
------------------

* GNU/Linux or MacOS

* [PANDAseq](https://github.com/neufeld/pandaseq)

* R >= 3.0 [R-project](http://cran.r-project.org/) with [BioConductor/ShortRead](http://www.bioconductor.org/packages/2.14/bioc/html/ShortRead.html)

* [seqtk](https://github.com/lh3/seqtk) to get the reverse complement (Include)


Paramenters
-----------------

-f, --forward  Forward fastq

-b, --backward  backward fastq

-o, --output output file name. Default: output.fastq

-l, --overlap  Minimun overlap. Default: 60

-m, --minimum  Minimun read length. Default: 200

-r, --reverse  Reverse complement

-s, --subset  Number of reads to sample


Example.
-----------------

> ./imm-illumina -f /home/user/forward.fastq  -b /home/user/backward.fastq -o /home/user/output.fastq -r -s 500000 -l 70 
